FROM python:3.6-alpine
LABEL maintainer="martinez.d.rodrigo@gmail.com"

RUN apk add --no-cache \
    --virtual=.build-dependencies \
    make \
    automake \
    gcc \
    g++ \
    gfortran \
    file \
    binutils \
    musl-dev \
    subversion \
    python3-dev \
    openblas-dev && \
    apk add libstdc++ openblas && \
    ln -s locale.h /usr/include/xlocale.h

# Create the group and user to be used in this container
RUN addgroup normangroup && adduser --home /home/norman --ingroup normangroup --disabled-password --shell /bin/bash norman

RUN mkdir -p /home/norman/address-parser
WORKDIR /home/norman/address-parser

COPY requirements.txt /home/norman/address-parser/
RUN pip install -r requirements.txt
RUN rm /usr/include/xlocale.h && apk del .build-dependencies
COPY . /home/norman/address-parser

RUN chown -R norman:normangroup /home/norman

USER norman