#!/usr/bin/python3
from iso3166 import countries


def validate_country_code(alpha_2):
    if not isinstance(alpha_2, str):
        raise TypeError("'%s' must be a string" % alpha_2)
    if not alpha_2.islower():
        raise ValueError("'%s' must be a lower case string" % alpha_2)

    try:
        countries.get(alpha_2)
    except KeyError as e:
        raise ValueError("'%s' must be a valid ISO 3166-1 alpha-2" % alpha_2) from e

