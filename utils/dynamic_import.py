#!/usr/bin/python3
from importlib import import_module


def dynamically_import(path, package=None, object_name=None):
    imported_module = import_module(name=path, package=package)
