from unittest import TestCase, mock
from utils.validators import validate_country_code


class ValidateCountryAlphaTest(TestCase):
    def test_method_exist(self):
        self.assertTrue(callable(validate_country_code))

    def test_raise_exception_if_alpha_2_isnt_a_string(self):
        self.assertRaises(TypeError, lambda: validate_country_code(1))

    def test_raise_exception_if_alpha_2_have_more_than_two_characteres(self):
        self.assertRaises(ValueError, lambda: validate_country_code("123"))

    def test_raise_exception_if_alpha_2_isnt_an_alpha_string(self):
        self.assertRaises(ValueError, lambda: validate_country_code("12"))

    @mock.patch("utils.validators.countries.get")
    def test_raise_exception_if_alpha_2_isnt_lower_case(self, countries_get_mock):
        self.assertRaises(ValueError, lambda: validate_country_code("AA"))
        countries_get_mock.assert_not_called()

    @mock.patch("utils.validators.countries")
    def test_raise_exception_if_alpha_2_isnt_a_valid_iso_3166_alpha_2(self, countries_mock):
        def raise_key_error(alpha_2):
            raise KeyError

        countries_mock.get = raise_key_error
        self.assertRaises(ValueError, lambda: validate_country_code("ff"))


