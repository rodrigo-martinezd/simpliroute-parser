from unittest import TestCase, mock, main
from utils.dynamic_import import dynamically_import


class DynamicallyImportTest(TestCase):

    def test_load_module_should_be_callable(self):
        self.assertTrue(callable(dynamically_import))

    @mock.patch("utils.dynamic_import.import_module")
    def test_import_module_has_call_with_module_path_and_package(self, import_module_mock):
        dynamically_import(path=".rules", package="cleaner.config.cl")
        import_module_mock.assert_called_once_with(name=".rules", package="cleaner.config.cl")

    @mock.patch("utils.dynamic_import.import_module")
    def test_raise_exception_if_path_is_relative_and_no_package_is_provided(self, import_module_mock):
        import_module_mock.side_effect = TypeError()
        self.assertRaises(TypeError, lambda: dynamically_import(path=".rules"))
