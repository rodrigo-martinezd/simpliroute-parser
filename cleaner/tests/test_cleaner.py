#!/usr/bin/python3
from unittest import main, TestCase, mock
from cleaner import Cleaner


class CleanerTest(TestCase):

    def test_raise_type_error_if_countries_supported_isnt_a_tuple(self):
        self.assertRaises(
            TypeError,
            lambda: Cleaner(countries_supported=123)
            )

    def test_raise_value_error_if_countries_supported_is_a_empty_tuple(self):
        self.assertRaises(
            ValueError,
            lambda: Cleaner(countries_supported=())
            )

    @mock.patch("cleaner.cleaner.validate_country_code")
    def test_raise_value_error_if_elements_in_countries_supported_arent_country_alpha2(self, mock_validate_country):
        mock_validate_country.side_effect = TypeError()

        self.assertRaises(
            ValueError,
            lambda: Cleaner(countries_supported=("aa",))
            )

    def test_that_countries_supported_cannot_be_override_from_outside(self):
        cleaner = Cleaner(("as",))

        def set_countries_supported():
            cleaner.countries_supported = ["cl"]

        self.assertRaises(
            SyntaxError,
            set_countries_supported
            )


if __name__ == "__main__":
    main()
