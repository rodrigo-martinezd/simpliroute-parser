#!/usr/bin/python3
from utils.validators import validate_country_code


class Cleaner(object):
    """
    Class dedicated to clean an address string from unused words and rare characters, providing methods to
    load cleaner configuration by country and perform the required transformations
    """

    def __init__(self, countries_supported):
        if not isinstance(countries_supported, tuple):
            raise TypeError("countries_supported must be a tuple")

        if len(countries_supported) == 0:
            raise ValueError("countries_supported can't be an empty tuple")

        try:
            for country_code in countries_supported:
                validate_country_code(country_code)

        except TypeError:
            raise ValueError("Some elements of countries_supported aren't valid ISO 3166-1 alpha-2")

        self.__countries_supported = countries_supported

        # TODO: Load dynamically modules inside config.{country_code}.rules

    @property
    def countries_supported(self):
        return self.__countries_supported

    @countries_supported.setter
    def countries_supported(self, value):
        raise SyntaxError("Can't assign countries_supported attribute")
